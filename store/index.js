export const state = () => ({
  destinations: [
    {
      id: null,
      name: 'Toutes',
    },
    {
      id: 1,
      name: 'Emirats Arabes Unis',
    },
    {
      id: 2,
      name: 'Seychelles',
    },
    {
      id: 3,
      name: 'Maurice',
    },
  ],
  cards: [
    {
      destinationId: 1,
      destination: 'Emirats Arabes Unis',
      place: 'Dubaï',
      images: [
        {
          imgSrc: require('~/assets/images/FAIRMONT_DUBAI.png'),
          imgAlt: 'FAIRMONT_DUBAI',
        },
        {
          imgSrc: require('~/assets/images/CLOS_DU_LITTORAL.png'),
          imgAlt: 'CLOS_DU_LITTORAL',
        },
        {
          imgSrc: require('~/assets/images/ENTRE_CULTURE_ET_PLAGES.png'),
          imgAlt: 'ENTRE_CULTURE_ET_PLAGES',
        },
      ],
      description: 'Metropolitan Hotel Dubaï',
      rate: 4,
      tags: ['Premium', 'Petit déjeuner'],
      price: "jusqu'à -70%",
    },
    {
      destinationId: 2,
      destination: 'Seychelles',
      place: 'Mahe',
      images: [
        {
          imgSrc: require('~/assets/images/CLOS_DU_LITTORAL.png'),
          imgAlt: 'CLOS_DU_LITTORAL',
        },
        {
          imgSrc: require('~/assets/images/HYATT_REGENCY_CREEK.png'),
          imgAlt: 'HYATT_REGENCY_CREEK',
        },
        {
          imgSrc: require('~/assets/images/NOKU_MALDIVES.png'),
          imgAlt: 'NOKU_MALDIVES',
        },
      ],
      description: 'Avani Barbarons Resort & Spa',
      rate: 3,
      tags: ['Grand confort', 'Dîner offer'],
      price: 'Dès 225€/pers',
    },
    {
      destinationId: 3,
      destination: 'Maurice',
      place: 'Trou aux biches',
      images: [
        {
          imgSrc: require('~/assets/images/ENTRE_CULTURE_ET_PLAGES.png'),
          imgAlt: 'ENTRE_CULTURE_ET_PLAGES',
        },
        {
          imgSrc: require('~/assets/images/HYATT_REGENCY_CREEK.png'),
          imgAlt: 'HYATT_REGENCY_CREEK',
        },
        {
          imgSrc: require('~/assets/images/FAIRMONT_DUBAI.png'),
          imgAlt: 'FAIRMONT_DUBAI',
        },
      ],
      description: 'La Palmiste',
      rate: 4,
      tags: ['Luxe', 'Spa'],
      price: "jusqu'à -50%",
    },
    {
      destinationId: 1,
      destination: 'Emirats Arabes Unis',
      place: 'Dubaï',
      images: [
        {
          imgSrc: require('~/assets/images/FAIRMONT_DUBAI.png'),
          imgAlt: 'FAIRMONT_DUBAI',
        },
        {
          imgSrc: require('~/assets/images/CLOS_DU_LITTORAL.png'),
          imgAlt: 'CLOS_DU_LITTORAL',
        },
        {
          imgSrc: require('~/assets/images/ENTRE_CULTURE_ET_PLAGES.png'),
          imgAlt: 'ENTRE_CULTURE_ET_PLAGES',
        },
      ],
      description: 'Metropolitan Hotel Dubaï',
      rate: 4,
      tags: ['Premium', 'Petit déjeuner'],
      price: "jusqu'à -70%",
    },
    {
      destinationId: 2,
      destination: 'Seychelles',
      place: 'Mahe',
      images: [
        {
          imgSrc: require('~/assets/images/NOKU_MALDIVES.png'),
          imgAlt: 'NOKU_MALDIVES',
        },
        {
          imgSrc: require('~/assets/images/HYATT_REGENCY_CREEK.png'),
          imgAlt: 'HYATT_REGENCY_CREEK',
        },
        {
          imgSrc: require('~/assets/images/ENTRE_CULTURE_ET_PLAGES.png'),
          imgAlt: 'ENTRE_CULTURE_ET_PLAGES',
        },
      ],
      description: 'Avani Barbarons Resort & Spa',
      rate: 3,
      tags: ['Grand confort', 'Dîner offer'],
      price: 'Dès 225€/pers',
    },
    {
      destinationId: 3,
      destination: 'Maurice',
      place: 'Trou aux biches',
      images: [
        {
          imgSrc: require('~/assets/images/IMPIANA_RESORT_SAMUI.png'),
          imgAlt: 'IMPIANA_RESORT_SAMUI',
        },
        {
          imgSrc: require('~/assets/images/HYATT_REGENCY_CREEK.png'),
          imgAlt: 'HYATT_REGENCY_CREEK',
        },
        {
          imgSrc: require('~/assets/images/FAIRMONT_DUBAI.png'),
          imgAlt: 'FAIRMONT_DUBAI',
        },
      ],
      description: 'La Palmiste',
      rate: 4,
      tags: ['Luxe', 'Spa'],
      price: "jusqu'à -50%",
    },
    {
      destinationId: 1,
      destination: 'Emirats Arabes Unis',
      place: 'Dubaï',
      images: [
        {
          imgSrc: require('~/assets/images/FAIRMONT_DUBAI.png'),
          imgAlt: 'FAIRMONT_DUBAI',
        },
        {
          imgSrc: require('~/assets/images/CLOS_DU_LITTORAL.png'),
          imgAlt: 'CLOS_DU_LITTORAL',
        },
        {
          imgSrc: require('~/assets/images/ENTRE_CULTURE_ET_PLAGES.png'),
          imgAlt: 'ENTRE_CULTURE_ET_PLAGES',
        },
      ],
      description: 'Metropolitan Hotel Dubaï',
      rate: 4,
      tags: ['Premium', 'Petit déjeuner'],
      price: "jusqu'à -70%",
    },
    {
      destinationId: 2,
      destination: 'Seychelles',
      place: 'Mahe',
      images: [
        {
          imgSrc: require('~/assets/images/NOKU_MALDIVES.png'),
          imgAlt: 'NOKU_MALDIVES',
        },
        {
          imgSrc: require('~/assets/images/JAPON.jpg'),
          imgAlt: 'JAPON',
        },
        {
          imgSrc: require('~/assets/images/ENTRE_CULTURE_ET_PLAGES.png'),
          imgAlt: 'ENTRE_CULTURE_ET_PLAGES',
        },
      ],
      description: 'Avani Barbarons Resort & Spa',
      rate: 3,
      tags: ['Grand confort', 'Dîner offer'],
      price: 'Dès 225€/pers',
    },
    {
      destinationId: 3,
      destination: 'Maurice',
      place: 'Trou aux biches',
      images: [
        {
          imgSrc: require('~/assets/images/IMPIANA_RESORT_SAMUI.png'),
          imgAlt: 'IMPIANA_RESORT_SAMUI',
        },
        {
          imgSrc: require('~/assets/images/NOKU_MALDIVES.png'),
          imgAlt: 'NOKU_MALDIVES',
        },
        {
          imgSrc: require('~/assets/images/FAIRMONT_DUBAI.png'),
          imgAlt: 'FAIRMONT_DUBAI',
        },
      ],
      description: 'La Palmiste',
      rate: 4,
      tags: ['Luxe', 'Spa'],
      price: "jusqu'à -50%",
    },
    {
      destinationId: 1,
      destination: 'Emirats Arabes Unis',
      place: 'Dubaï',
      images: [
        {
          imgSrc: require('~/assets/images/FAIRMONT_DUBAI.png'),
          imgAlt: 'FAIRMONT_DUBAI',
        },
        {
          imgSrc: require('~/assets/images/CLOS_DU_LITTORAL.png'),
          imgAlt: 'CLOS_DU_LITTORAL',
        },
        {
          imgSrc: require('~/assets/images/ENTRE_CULTURE_ET_PLAGES.png'),
          imgAlt: 'ENTRE_CULTURE_ET_PLAGES',
        },
      ],
      description: 'Metropolitan Hotel Dubaï',
      rate: 4,
      tags: ['Premium', 'Petit déjeuner'],
      price: "jusqu'à -70%",
    },
    {
      destinationId: 2,
      destination: 'Seychelles',
      place: 'Mahe',
      images: [
        {
          imgSrc: require('~/assets/images/NOKU_MALDIVES.png'),
          imgAlt: 'NOKU_MALDIVES',
        },
        {
          imgSrc: require('~/assets/images/JAPON.jpg'),
          imgAlt: 'JAPON',
        },
        {
          imgSrc: require('~/assets/images/ENTRE_CULTURE_ET_PLAGES.png'),
          imgAlt: 'ENTRE_CULTURE_ET_PLAGES',
        },
      ],
      description: 'Avani Barbarons Resort & Spa',
      rate: 3,
      tags: ['Grand confort', 'Dîner offer'],
      price: 'Dès 225€/pers',
    },
    {
      destinationId: 3,
      destination: 'Maurice',
      place: 'Trou aux biches',
      images: [
        {
          imgSrc: require('~/assets/images/JAPON.jpg'),
          imgAlt: 'JAPON',
        },
        {
          imgSrc: require('~/assets/images/NOKU_MALDIVES.png'),
          imgAlt: 'NOKU_MALDIVES',
        },
        {
          imgSrc: require('~/assets/images/FAIRMONT_DUBAI.png'),
          imgAlt: 'FAIRMONT_DUBAI',
        },
      ],
      description: 'La Palmiste',
      rate: 4,
      tags: ['Luxe', 'Spa'],
      price: "jusqu'à -50%",
    },
  ],
  filteredCards: [],
});

export const getters = {
  getDestinations(state) {
    return state.destinations;
  },
  getCards(state) {
    if (state.filteredCards.length) {
      return state.filteredCards;
    }
    return state.cards;
  },
};

export const mutations = {
  selectDestination(state, destinationId) {
    if (destinationId === 0) {
      state.filteredCards = state.cards;
    } else {
      state.filteredCards = state.cards.filter(card => card.destinationId === destinationId);
    }
  },
};
